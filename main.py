# import
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import euclidean_distances
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import sys

def read_words(words_file):
    return [word for line in open(words_file, 'r') for word in line.split()]

# init vectorizers
tfidf_vectorizer = TfidfVectorizer(use_idf=True)
tf_vectorizer = TfidfVectorizer(use_idf=False)
binary_vectorizer = CountVectorizer(binary=True)

# prepare corpus
corpus = []
for d in range(1400):
    f = open("./d/" +str(d+1) +".txt")
    corpus.append(f.read())
# add query to corpus

outStats = open("stats.txt", "w")
outRes = open("results.txt", "w")
orig_stdout = sys.stdout
sys.stdout = outRes

for q in range(225):
    f = open("./q/" +str(q+1) +".txt")
    relevant = read_words("./r/" + str(q+1) + ".txt")
    relLen = relevant.__len__()
    #maxRes = 15
    maxRes = relLen
    curCorpus = corpus
    curCorpus.append(f.read())

    # prepare matrix
    tfidf_matrix = tfidf_vectorizer.fit_transform(curCorpus)
    tf_matrix = tf_vectorizer.fit_transform(curCorpus)
    binary_matrix = binary_vectorizer.fit_transform(curCorpus)

    # compute similarity between query and all docs (tf-idf) and get top 10 relevant
    cosSimTF = np.array(cosine_similarity(tf_matrix[len(curCorpus ) -1], tf_matrix[0:(len(curCorpus ) -1)])[0])
    cosSimtfIDF = np.array(cosine_similarity(tfidf_matrix[len(curCorpus ) -1], tfidf_matrix[0:(len(curCorpus ) -1)])[0])
    cosSimBin = np.array(cosine_similarity(binary_matrix[len(curCorpus ) -1], binary_matrix[0:(len(curCorpus ) -1)])[0])
    eucSimTF = np.array(euclidean_distances(tf_matrix[len(curCorpus ) -1], tf_matrix[0:(len(curCorpus ) -1)])[0])
    eucSimtfIDF = np.array(euclidean_distances(tfidf_matrix[len(curCorpus ) -1], tfidf_matrix[0:(len(curCorpus ) -1)])[0])
    eucSimBin = np.array(euclidean_distances(binary_matrix[len(curCorpus) - 1], binary_matrix[0:(len(curCorpus) - 1)])[0])
    trcTF = cosSimTF.argsort()[-maxRes:][::-1 ] +1          #1
    trcTFidf = cosSimtfIDF.argsort()[-maxRes:][::-1 ] +1    #2
    trcBin = cosSimBin.argsort()[-maxRes:][::-1 ] +1        #3
    treTF = eucSimTF.argsort()[:maxRes] +1                  #4
    treTFidf = eucSimtfIDF.argsort()[:maxRes ] +1           #5
    treBin = eucSimBin.argsort()[:maxRes ] +1               #6

    tp1 = 0
    for i in trcTF:
        if str(i) in relevant:
            tp1 = tp1 + 1
    fp1 = maxRes - tp1
    fn1 = relLen - tp1

    tp2 = 0
    for i in trcTFidf:
        if str(i) in relevant:
            tp2 = tp2 + 1
    fp2 = maxRes - tp2
    fn2 = relLen - tp2

    tp3 = 0
    for i in trcBin:
        if str(i) in relevant:
            tp3 = tp3 + 1
    fp3 = maxRes - tp3
    fn3 = relLen - tp3

    tp4 = 0
    for i in treTF:
        if str(i) in relevant:
            tp4 = tp4 + 1
    fp4 = maxRes - tp4
    fn4 = relLen - tp4

    tp5 = 0
    for i in treTFidf:
        if str(i) in relevant:
            tp5 = tp5 + 1
    fp5 = maxRes - tp5
    fn5 = relLen - tp5

    tp6 = 0
    for i in treBin:
        if str(i) in relevant:
            tp6 = tp6 + 1
    fp6 = maxRes - tp6
    fn6 = relLen - tp6

    prec1 = tp1/float((tp1+fp1))       #beacuse i always try to quess same number of relevant documents as is in rXXX.txt, the precision and recall will always be the same
    rec1 = tp1/float((tp1+fn1))
    prec2 = tp2 / float((tp2 + fp2))
    rec2 = tp2 / float((tp2 + fn2))
    prec3 = tp3 / float((tp3 + fp3))
    rec3 = tp3 / float((tp3 + fn3))
    prec4 = tp4 / float((tp4 + fp4))
    rec4 = tp4 / float((tp4 + fn4))
    prec5 = tp5 / float((tp5 + fp5))
    rec5 = tp5 / float((tp5 + fn5))
    prec6 = tp6 / float((tp6 + fp6))
    rec6 = tp6 / float((tp6 + fn6))

    fm1 = fm2 = fm3 = fm4 = fm5 = fm6 = 0
    if (prec1 + rec1 != 0):
        fm1 = 2 * ((prec1 * rec1) / float((prec1 + rec1)))
    if (prec2 + rec2 != 0):
        fm2 = 2 * ((prec2 * rec2) / float((prec2 + rec2)))
    if (prec3 + rec3 != 0):
        fm3 = 2 * ((prec3 * rec3) / float((prec3 + rec3)))
    if (prec4 + rec4 != 0):
        fm4 = 2 * ((prec4 * rec4) / float((prec4 + rec4)))
    if (prec5 + rec5 != 0):
        fm5 = 2 * ((prec5 * rec5) / float((prec5 + rec5)))
    if (prec6 + rec6 != 0):
        fm6 = 2 * ((prec6 * rec6) / float((prec6 + rec6)))



    print(q+1)
    print('Relevant documents', relevant)
    print('cos, tf', trcTF)
    print('cos, tfidf', trcTFidf)
    print('cos, bin', trcBin)
    print('euc, tf', treTF)
    print('euc, tfidf', treTFidf)
    print('euc, bin', treBin)
    print('\n\n')

    outStats.write(str(q+1) + ' ' + str(prec1) + ' ' + str(rec1) + ' ' + str(fm1) + ' ' + str(prec2) + ' ' + str(rec2) + ' ' + str(fm2) + ' ' + str(prec3) + ' ' + str(rec3)+ ' ' + str(fm3) + ' ' + str(prec4) + ' ' + str(rec4) + ' ' + str(fm4) + ' ' + str(prec5) + ' ' + str(rec5) + ' ' + str(fm5) + ' ' + str(prec6) + ' ' + str(rec6) + ' ' + str(fm6) + '\n')